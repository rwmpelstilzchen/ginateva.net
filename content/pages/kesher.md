title: יצירת קשר
slug: kesher

בכל הנוגע לגינה הקהילתית אתם מוזמנים לפנות אל **אמנון הרציג** בכתובת <a href="mailto:amnon.herzig@gmail.com"><span style="font-family: monospace">amnon.herzig@gmail.com</span></a>. זו גם הכתובת לפניות בכל עניין וגם הכתובת לבקשה להצטרפות לרשימת־התפוצה.

**הגינה פתוחה** באופן חופשי בימי א׳–ה׳ ברוב שעות היום ובשבת לפני הצהריים. כתובתהּ היא [רח׳ המגיד בירושלים, מול בית מס׳ 6](http://www.mapa.co.il/maps?CurrMapTab=5.1&MapType=map&ImagesOption=&CurrHeaderTab=2&RoutePoints=1x3000x%u05D9%u05E8%u05D5%u05E9%u05DC%u05D9%u05DDx%u05D4%u05DE%u05D2%u05D9%u05D3x6%2C&UserEarthX=170957&UserEarthY=1130333&UserMapZoom=500&LoadUserPosition=1&RouteType=2&PNGLayers=) (הכניסה היא ממגרש החניה). קווי־האוטובוס המגיעים לגינה (יורדים בתחנה עמק רפאים × לויד ג׳ורג׳): 18, 18א, 34, 34א, 77, 77א. בכל יום חמישי, החל מ־15:00 בחורף ו־16:00 בקיץ, נפגשים בגינה לעבודה משותפת; אתם מאוד מוזמנים ☺

בעניינים הנוגעים ל**אתר**, פנו אל [יודה רונן](http://me.digitalwords.net/). האתר בנוי על מערכת [פליקן](http://getpelican.com/) ומערכת שבניתי, [mail2staticsite](https://gitlab.com/rwmpelstilzchen/mail2staticsite), שמטרתה להמיר הודעות דואל (במקרה דנן, הודעות שאמנון שולח) לקבצים שפליקן ומערכות דומות יכולות לעבד. קבצי המקור של האתר זמינים [כאן](https://gitlab.com/rwmpelstilzchen/ginateva.net).
