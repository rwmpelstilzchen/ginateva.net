Title: בגינה
slug: gina

<ul>
	<li><a href="/bejtit"><i class="icon-droplet"></i>מִתקני הדגמה של חקלאות ביתית</a></li>
	<li><i class="icon-tree-2"></i>הכרות עם עצי הגינה:
		<ul>
			<li><a href="/ecim-jeladim"><i class="icon-child"></i>גן הפלאים: משחק הפעלה לילדים ומבוגרים בנושא עצים בגינה הקהילתית</a></li>
			<li><a href="/ecim-mevugarim"><i class="icon-binoculars"></i>סיור בעקבות עצים בגינה הקהילתית: מדריך לטיול עצמאי</a></li>
		</ul>
	</li>
	<li><a href="/brexot"><i class="icon-air"></i>ארבע בריכות המים בגינה</a></li>
	<li><a href="/mucarim"><i class="icon-gift"></i>מוצרים המיוצרים מהצמחים הגדלים בגינה</a> (נמכרים בגינה או בהזמנה)</li>
</ul>

<i class="icon-calendar"></i>בנוסף, אתם מוזמנים לקחת חלק ב[פעילות בגינה](/peilut).

<center><iframe width="560" height="420" src="https://www.youtube.com/embed/dSaAAQPgE7k" frameborder="0" allowfullscreen></iframe></center>
