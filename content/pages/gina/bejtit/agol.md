title: אוהל עגול
slug: agol

<table><tr><td class="hodaa">עמוד זה מציג <strong>מִתקן יחיד</strong> מתוך סקירת <a href="{filename}index.md">מתקני ההדגמה לחקלאות ביתית</a> שבגינה.</td></tr></table>

**האוהל הקהילתי** הוקם בהשראת ה־Roundhouse — מבנה עגול באנגליה וסקוטלנד בתקופות הברונזה והברזל ועד התקופה הרומית. המבנה הקדום, שקוטרו 15–5 מטר, הורכב מאבנים או ממוטות עץ וכוסה בגג של קש.

<div><img src="{filename}/images/bejtit/agol-2.jpg" width="50%"/><img src="{filename}/images/bejtit/agol-1.jpg" width="50%"/></div>
<center>
בנייה מסורתית קלטית של ראונדהאוס.<br/>
מימין: מבנה עם שלד עשוי כולו מעץ בעת הבנייה.<br/>
משמאל: מבנה גמור שבו הקירות עשויים אבן, שלד הגג עשוי ממוטות עץ ומכוסה קש.
</center>



האוהל בגינה הקהילתית נבנה מבמבוק בטכניקה הדומה לזו המקורית, תוך התאמה לתנאי הגינה ולצרכים הקהילתיים של פעיליה. השלד פחות מסיבי, היות שאינו נושא משקל רב ואינו מיועד למגורים בשונה ממבני הראונדהאוס הקדומים באירופה. המבנה הוקם על ידי פעילי הגינה בעזרת מומחים לבנייה בבמבוק.

סרט המתעד את הקמת האוהל:

<center><iframe width="560" height="315" src="//www.youtube.com/embed/FW5IkPEcYFk" frameborder="0" allowfullscreen></iframe></center>

שתי התמונות מציגות שלבים בבניית האוהל:

[![שלד הבמבוק]({filename}/images/bejtit/agol-3.jpg)]({filename}/images/bejtit/agol-3.jpg)

[![עבודה קהילתית]({filename}/images/bejtit/agol-4.jpg)]({filename}/images/bejtit/agol-4.jpg)

האוהל הבנוי:

[![… כל העניין הוא לנוח בצל בתוך האוהל]({filename}/images/bejtit/agol-5.jpg)]({filename}/images/bejtit/agol-5.jpg)

פעילות באוהל:

<iframe width="560" height="315" src="https://www.youtube.com/embed/3vs79pRgiQA" frameborder="0" allowfullscreen></iframe>
