#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


AUTHOR = 'הגינה הקהילתית'
SITENAME = 'הגינה הקהילתית במוזיאון הטבע'
SITEURL = 'http://localhost:8000'

TIMEZONE = 'Asia/Jerusalem'

DEFAULT_LANG = 'he'

# Feed generation is usually not desired when developing
# FEED_ALL_ATOM = None
# CATEGORY_FEED_ATOM = None
# TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = "themes/gentle"

# DATE_FORMAT={'he': '%Y-%m-%d(%a)'}
DEFAULT_DATE_FORMAT = ('‎%Y·%m·%d‏')
LOCALE = ('he_IL.utf8')
FILENAME_METADATA = ('(?P<date>\d{4}-\d{2}-\d{2})-(?P<slug>.*)')

PLUGIN_PATHS = ['/home/me/documents/www/ginateva.net/pelican-plugins']
# PLUGINS = ['gallery']

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False


PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'
ARTICLE_URL = PAGE_URL
ARTICLE_SAVE_AS = PAGE_SAVE_AS

STATIC_PATHS = ['images', 'pdfs']
